import React, { Component } from "react";
import { Layout, Menu, message, Badge, Modal, Button, Icon } from "antd";
import RouteMenu from "./routeMenu";
import { connect } from "react-redux";
import { CopyToClipboard } from "react-copy-to-clipboard";

const { Header, Content, Footer } = Layout;
const topics = ["home", "favorite"];
// const topics = ["home", "favorite", "profile"];
const mapStateToProps = state => {
  return {
    isShowDialog: state.isShowDialog,
    itemGiphy: state.itemGiphy,
    AnItem: state.AnItem
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDismissDialog: () =>
      dispatch({
        type: "dismiss_dialog"
      }),
    onClickItemGiphy: item =>
      dispatch({
        type: "click_item",
        payload: item
      })
  };
};

class MainPage extends Component {
  state = {
    items: [],
    curTopic: "",
    favGiphy: [],
    email: "",
    isCopies: false,
    item: {}
  };
  showDialogToConfirmLogout = () => {
    this.setState({ isShowDialog: true });
  };
  onClickCopyToClipboard(value) {
    message.success(`Copied ${value}`);
    this.props.onDismissDialog();
  }

  componentDidMount() {
    const jsonStr = localStorage.getItem("user-info");
    const email = jsonStr && JSON.parse(jsonStr).email;
    const { pathname } = this.props.location;
    let curTopic = topics[0]; // path var that can change to any
    if (pathname != "/") {
      curTopic = pathname.replace("/", "");
      if (!topics.includes(curTopic)) curTopic = topics[0];
    }
    const jsonFavTOStr = localStorage.getItem(`favorite-giphy-list${email}`);
    if (jsonFavTOStr) {
      const items = jsonFavTOStr && JSON.parse(jsonFavTOStr);
      this.setState({ favGiphy: items });
    }
    const jsonCopyTOStr = localStorage.getItem(`copy-giphy-item${email}`);
    if (jsonCopyTOStr) {
      const item = jsonFavTOStr && JSON.parse(jsonCopyTOStr);
      this.setState({ item: item });
    }
    this.setState({ curTopic, email });
  }

  onClickMenu = e => {
    if (e.key) {
      var path = "/";
      path = `/${e.key}`;
      this.props.history.replace(path);
    }
  };

  onClickFavoriteGif = () => {
    const items = this.state.favGiphy;
    const itemGiphy = this.props.itemGiphy;
    const index = items.findIndex(item => {
      return item.title === itemGiphy.title;
    });
    if (index != -1) {
      items.splice(index, 1);
      localStorage.setItem(
        `favorite-giphy-list${this.state.email}`,
        JSON.stringify(items)
      );
      message.error("Removed from favorite's list ", 0.5, () => {
        this.setState({ favGiphy: items });
        this.onModalClickCancel();
      });
    } else {
      items.push(itemGiphy);
      localStorage.setItem(
        `favorite-giphy-list${this.state.email}`,
        JSON.stringify(items)
      );
      message.success("Saved this gif to your favorite's list", 0.5, () => {
        this.setState({ favGiphy: items });
        this.onModalClickCancel();
      });
    }
  };
  onModalClickCancel = () => {
    this.props.onDismissDialog();
  };

  checkItemFavorited = () => {
    const items = this.state.favGiphy;
    const itemGiphy = this.props.itemGiphy;
    const result = items.find(item => {
      return item.title === itemGiphy.title;
    });
    if (result) {
      return "primary";
    } else {
      return "";
    }
  };

  handleOk = () => {
    localStorage.setItem(
      "user-info",
      JSON.stringify({
        isLoggedIn: false
      })
    );
    this.props.history.push("/");
  };
  handleCancel = () => {
    this.setState({ isShowDialog: false });
  };

  render() {
    const itemGiphy = this.props.itemGiphy;
    console.log(itemGiphy);
    return (
      <div style={{ height: "100vh" }}>
        {" "}
        <Layout className="layout" style={{ background: "white" }}>
          <Header
            theme="light"
            style={{
              padding: "0px",
              position: "fixed",
              zIndex: 1,
              width: "100%"
            }}
          >
            <Menu
              theme="light"
              mode="horizontal"
              style={{ height: "100%", width: "100%" }}
              defaultSelectedKeys={[this.state.curTopic]}
              onClick={e => {
                this.onClickMenu(e);
              }}
            >
              <div
                style={{
                  position: "absolute",
                  top: "50%",
                  left: "10%",
                  transform: "translateY(-40%)"
                }}
              >
                <div>
                  <h1>GIPHY</h1>
                </div>
              </div>
              <Menu.Item key={topics[0]}>Home</Menu.Item>
              <Menu.Item key={topics[1]}>Favorite</Menu.Item>
              {/* <Menu.Item key={topics[2]}>Profile</Menu.Item> */}
              <div
                style={{
                  position: "absolute",
                  top: "50%",
                  right: "46px",
                  transform: "translateY(-40%)"
                }}
              >
                <Button
                  type="danger"
                  size="large"
                  style={{
                    background: "red",
                    cursor: "pointer",
                    border: "none"
                  }}
                  onClick={this.showDialogToConfirmLogout}
                >
                  <Icon
                    type="logout"
                    style={{ fontSize: "25px", color: "white" }}
                  />
                </Button>
              </div>
            </Menu>
          </Header>
          <Content
            style={{
              padding: "16px",
              minHeight: "600px",
              marginTop: 24,
              justifyContent: "center",
              alignItems: "center",
              display: "flex"
            }}
          >
            <RouteMenu items={this.state.items} />
          </Content>
          <Footer style={{ textAlign: "center", background: "white" }}>
            <img alt="powerByGiphy" src={require("./giphy.png")} />
          </Footer>
        </Layout>
        {itemGiphy != null ? (
          <Modal
            width="40%"
            style={{ maxHeight: "70%", top: "20%" }}
            title={itemGiphy.title}
            visible={this.props.isShowDialog}
            onCancel={this.onModalClickCancel}
            footer={[
              <Button
                key="submit"
                type={this.checkItemFavorited()}
                icon="heart"
                size="large"
                shape="circle"
                type="primary"
                style={{
                  color: "white",
                  backgroundColor: " red",
                  border: "none"
                }}
                onClick={this.onClickFavoriteGif}
              />,
              <CopyToClipboard
                text={`https://media.giphy.com/media/${itemGiphy.id}/giphy.gif`}
                onCopy={() => this.setState({ copied: true })}
              >
                <Button
                  key="submit2"
                  type="primary"
                  icon="paper-clip"
                  size="large"
                  shape="circle"
                  style={{
                    color: "black",
                    backgroundColor: " yellow",
                    border: "none"
                  }}
                  onClick={() => {
                    this.onClickCopyToClipboard(itemGiphy.url);
                  }}
                />
              </CopyToClipboard>
            ]}
          >
            <p>Choose your actions: Add favorite or copy to clipboard </p>
          </Modal>
        ) : (
          <div />
        )}
        <Modal
          title="Logout"
          visible={this.state.isShowDialog}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <p>Are you sure to logout?</p>
        </Modal>
      </div>
    );
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainPage);
