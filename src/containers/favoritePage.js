import React, { Component } from "react";
import ListFavorite from "../components/favorite/list";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return {
    isShowDialog: state.isShowDialog
  };
};

class FavoritePage extends Component {
  getItems = () => {
    const jsonStr = localStorage.getItem("user-info");
    const email = jsonStr && JSON.parse(jsonStr).email;
    const jsonFavStr = localStorage.getItem(`favorite-giphy-list${email}`);
    if (jsonFavStr) {
      const items = jsonFavStr && JSON.parse(jsonFavStr);
      return items;
    }
  };

  render() {
    return (
      <div
        style={{
          padding: "16px",
          marginTop: 64,
          minHeight: "600px"
        }}
      >
        <ListFavorite items={this.getItems()} />
      </div>
    );
  }
}

export default connect(mapStateToProps)(FavoritePage);
