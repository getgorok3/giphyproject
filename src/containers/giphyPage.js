import React, { Component } from "react";
import { Spin, Input, Col, Row, Button, message, List, Radio } from "antd";
import ListGiphy from "../components/giphy/listGiphy";
import TextTruncate from "react-text-truncate";

const Search = Input.Search;
const GphApiClient = require("giphy-js-sdk-core");
const ApiKey = "OJAjx7SEJNX91V9tN9afjjHH2zyhP29p";
const client = GphApiClient("OJAjx7SEJNX91V9tN9afjjHH2zyhP29p");

const categories = [
  "Entertainment",
  "Sports",
  "Animals",
  "Food",
  "Memes",
  "Emotions",
  "Anime",
  "Gaming"
];

class GiphyPage extends Component {
  state = {
    items: [],
    page: 1,
    perPage: 40,
    favItems: [],
    isFav: false
  };

  componentDidMount() {
    this.loadData();
  }

  loadData = () => {
    fetch(`http://api.giphy.com/v1/gifs/trending?api_key=${ApiKey}&limit=400`)
      .then(response => response.json())
      .then(items => {
        this.setState({ items: items.data });
      });
  };

  onSearchGif(searchValue) {
    console.log(searchValue);
    if (searchValue.trim().length === 0) {
      searchValue = "";
    }
    if (searchValue != "" && searchValue != null && searchValue != {}) {
      client
        .search("gifs", { q: searchValue, limit: 100 })
        .then(response => {
          if (response.data.length !== 0) {
            console.log(response.data.length);
            this.setState({ items: response.data });
          } else {
            message.error("No data", 1);
            this.loadData();
          }
        })
        .catch(err => {});
    } else {
      message.success(`Return to Home Page`);
      this.loadData();
    }
  }

  onButtonlistClick(value) {
    message.success(`Search by using keyword: ${value}`);
    client
      .gifsByCategories("tv", value, { limit: 100 })
      .then(response => {
        if (response.data.length !== 0) {
          this.setState({ items: response.data });
        } else {
          message.error("No data", 1);
          this.loadData();
        }
      })
      .catch(err => {
        message.error("Error", 3);
        this.loadData();
      });
  }

  render() {
    const itemGiphy = this.state.items;
    return (
      <div
        style={{
          padding: "16px",
          marginTop: 32,
          minHeight: "600px",
          justifyContent: "center",
          alignItems: "center",
          display: "flex"
        }}
      >
        {itemGiphy.length > 0 ? (
          <div>
            <Row>
              <Col style={{ marginBottom: 16 }}>
                <Search
                  style={{ width: "80%" }}
                  placeholder="Find Gif"
                  enterButton="Search"
                  size="large"
                  onSearch={value => this.onSearchGif(value)}
                />
              </Col>
            </Row>
            <Row>
              <Col style={{ marginBottom: 16 }}>
                <List
                  grid={{ column: 8, gutter: 8 }}
                  dataSource={categories}
                  renderItem={item => (
                    <List.Item>
                      <TextTruncate
                        style={{ cursor: "pointer", color: "#eb2f96" }}
                        line={1}
                        truncateText="…"
                        text={item}
                        onClick={e => {
                          this.onButtonlistClick(item);
                        }}
                      />
                    </List.Item>
                  )}
                />
              </Col>
            </Row>
            <Row>
              <ListGiphy items={this.state.items} />
            </Row>
          </div>
        ) : (
          <Spin size="large" tip="Loading..." />
        )}
      </div>
    );
  }
}

export default GiphyPage;
