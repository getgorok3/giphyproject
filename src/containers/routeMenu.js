import React from "react";
import { Route, Switch } from "react-router-dom";
import GiphyPage from "./giphyPage";
import FavoriteGiphyPage from "./favoritePage";
import ProfilePage from "../components/profile";
function Routes() {
  return (
    <div style={{ width: "100%" }}>
      <Switch>
        <Route exact path="/home" component={GiphyPage} />
        <Route exact path="/favorite" component={FavoriteGiphyPage} />
        {/* <Route exact path="/profile" component={ProfilePage} /> */}
      </Switch>
    </div>
  );
}

export default Routes;
