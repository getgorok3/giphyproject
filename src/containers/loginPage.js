import React, { Component } from "react";
import { auth, provider } from "../firebase";
import { Form, Button, Input, Icon, Divider, message, Modal } from "antd";

const bg = require("./comp_7.gif");
class LoginPage extends Component {
  state = {
    isLoading: false,
    email: "",
    password: "",
    isShowModal: false,
    isLogin: false,
    photoUrl: ""
  };
  componentDidMount() {
    const jsonStr = localStorage.getItem("user-info");
    const isLoggedIn = jsonStr && JSON.parse(jsonStr).isLoggedIn;
    if (isLoggedIn) {
      this.navigateToMainPage();
    }
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  navigateToMainPage = () => {
    const { history } = this.props;
    history.push("/home");
  };

  saveUserInformation = email => {
    localStorage.setItem(
      "user-info",
      JSON.stringify({
        email: email,
        isLoggedIn: true,
        photoUrl: this.state.photoUrl
      })
    );
    this.setState({ isLoading: false });
    this.navigateToMainPage();
  };

  onChangeEmail = event => {
    const email = event.target.value;
    this.setState({ email });
  };

  onChangePassword = event => {
    const password = event.target.value;
    this.setState({ password });
  };

  onClickLoginWithFacebook = () => {
    this.setState({ isLoading: true });
    auth.signInWithPopup(provider).then(({ user }) => {
      this.setState({ photoUrl: `${user.photoURL}?height=500` });
      console.log(user);
      this.saveUserInformation(user.email);
    });
  };

  onSubmitLoginForm = e => {
    e.preventDefault();
    this.setState({ isLogin: true });
    const email = this.state.email;
    const password = this.state.password;
    const isEailValid = this.validateEmail(email);
    if (isEailValid) {
      auth
        .signInWithEmailAndPassword(email, password)
        .then(({ user }) => {
          this.setState({ isLogin: false });
          this.saveUserInformation(user.email);
        })
        .catch(_ => {
          this.setState({ isLogin: false });
          this.setState({ isShowModal: true });
          message.error("The account not match", 1);
        });
    } else {
      this.setState({ isLogin: false });
      message.error("Email or Password invalid!", 1);
    }
  };

  handleCancel = () => {
    this.setState({ isShowModal: false, isLogin: false });
  };

  handleOk = () => {
    this.setState({ isShowModal: false, isLogin: false });
    this.props.history.push("/register");
  };

  navigateToRegisterPage = () => {
    const { history } = this.props;
    history.push("/register");
  };

  render() {
    return (
      <div
        style={{
          width: "30%",
          margin: "0 auto",
          paddingTop: "5%"
        }}
      >
        <img
          style={{
            width: "30%",
            height: " 30%",
            backgroundSize: "cover",
            overflow: "hidden",
            backgroundPosition: "center",
            borderRadius: "70%"
          }}
          src={bg}
        />
        <h1 style={{ color: "#9254de" }}>Welcome to GIPHY</h1>

        <Form onSubmit={this.onSubmitLoginForm}>
          <Form.Item>
            <Input
              prefix={<Icon type="user" style={{ color: "#faad14" }} />}
              placeholder="Email"
              onChange={this.onChangeEmail}
            />
          </Form.Item>

          <Form.Item>
            <Input
              prefix={<Icon type="lock" style={{ color: "#faad14" }} />}
              type="password"
              placeholder="Password"
              onChange={this.onChangePassword}
            />
          </Form.Item>

          <Form.Item>
            <Button
              style={{
                width: "44%",
                backgroundColor: "#eb2f96",
                border: "none"
              }}
              htmlType="submit"
              type="primary"
              loading={this.state.isLogin}
            >
              Login
            </Button>
          </Form.Item>

          <Form.Item>
            <Button
              style={{
                width: "45%",
                backgroundColor: "white",
                border: "none"
              }}
              htmlType="submit"
              loading={this.state.isLogin}
              onClick={this.navigateToRegisterPage}
            >
              Register
            </Button>
          </Form.Item>
        </Form>

        <Divider />

        <Button
          type="primary"
          icon="facebook"
          loading={this.state.isLoading}
          onClick={this.onClickLoginWithFacebook}
          style={{ backgroundColor: "#061178" }}
        >
          Login With Facebook
        </Button>

        <Modal
          title="Something went wrong"
          visible={this.state.isShowModal}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          footer={[
            <Button key="back" onClick={this.handleCancel}>
              No
            </Button>,
            <Button key="submit" type="primary" onClick={this.handleOk}>
              Yes
            </Button>
          ]}
        >
          <p>Sorry! not found this account but you want to create account.</p>
        </Modal>
      </div>
    );
  }
}
export default LoginPage;
