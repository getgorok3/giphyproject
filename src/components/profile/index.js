import React, { Component } from "react";
import { Modal } from "antd";

class ProfilePage extends Component {
  state = {
    email: "",
    isShowDialog: false,
    isLoading: false
  };

  componentDidMount() {
    const jsonStr = localStorage.getItem("user-info");
    const email = jsonStr && JSON.parse(jsonStr).email;
    this.setState({ email: JSON.parse(jsonStr).email });
  }

  showDialogToConfirmLogout = () => {
    this.setState({ isShowDialog: true });
  };

  handleOk = () => {
    localStorage.setItem(
      "user-info",
      JSON.stringify({
        isLoggedIn: false
      })
    );
    this.props.history.push("/");
  };
  handleCancel = () => {
    this.setState({ isShowDialog: false });
  };

  render() {
    return (
      <div>
        <h1>Email: {this.state.email}</h1>
        <button
          style={{
            padding: "10px 20px 10px 20px",
            background: "red",
            color: "white",
            fontSize: "18px",
            cursor: "pointer",
            border: "none",
            borderRadius: "10px"
          }}
          onClick={this.showDialogToConfirmLogout}
        >
          Logout
        </button>

        <Modal
          title="Logout"
          visible={this.state.isShowDialog}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <p>Are you sure to logout?</p>
        </Modal>
      </div>
    );
  }
}
export default ProfilePage;
