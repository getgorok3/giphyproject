import React, { Component } from "react";
import { List } from "antd";
import ItemGIphy from "../giphy/itemGiphy";

let user = "";
class FavoriteGiphyList extends Component {
  getUser() {
    const jsonStr = localStorage.getItem("user-info") || [];
    user = JSON.parse(jsonStr);
    if (user.photoUrl != null && user.photoUrl != "" && user.photoUrl != []) {
      console.log(user.email);
    } else {
      user.photoUrl =
        "https://upload.wikimedia.org/wikipedia/commons/d/d3/User_Circle.png";
    }
  }
  render() {
    this.getUser();
    console.log(user);
    return (
      <div>
        <div>
          <h3>Welcome !!! This page show your favorite gif list</h3>
          <img
            src={user.photoUrl}
            style={{
              width: 100,
              height: 100,
              padding: "2px",
              marginBottom: "20px"
            }}
          />
        </div>
        <List
          grid={{ gutter: 16, column: 4 }}
          dataSource={this.props.items}
          renderItem={item => (
            <List.Item>
              <ItemGIphy item={item} />
            </List.Item>
          )}
        />
      </div>
    );
  }
}

export default FavoriteGiphyList;
