import React from "react";
import { Card, Spin } from "antd";
import { connect } from "react-redux";
import ImageLoader from "react-load-image";

const mapDispatchToProps = dispatch => {
  return {
    onClickItemGiphy: item =>
      dispatch({
        type: "click_item",
        payload: item
      })
  };
};

function Preloader() {
  return <Spin size="large" style={{ marginTop: "25%" }} />;
}

function ItemGIphy(props) {
  return (
    <div>
      <Card
        style={{
          height: 200,
          padding: "2%",
          overflow: "hidden",
          backgroundColor: "#000000"
        }}
        hoverable
        onClick={() => {
          props.onClickItemGiphy(props.item);
        }}
        cover={
          <ImageLoader src={props.item.images.original.url}>
            <img style={{ height: "100%", width: "100%", objectFit: "fill" }} />
            {""}
            <Preloader />
          </ImageLoader>
        }
      />
    </div>
  );
}

export default connect(
  null,
  mapDispatchToProps
)(ItemGIphy);
