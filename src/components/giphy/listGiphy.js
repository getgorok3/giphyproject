import React, { Component } from "react";
import { List } from "antd";
import ItemGIphy from "./itemGiphy";

class ListGiphy extends Component {
  render() {
    return (
      <div style={{ minHeight: "300px" }}>
        <List
          grid={{ gutter: 16, column: 4 }}
          pagination={{
            pageSize: 40
          }}
          dataSource={this.props.items}
          renderItem={item => (
            <List.Item>
              <ItemGIphy
                item={item}
                clickFavFucn={this.props.onClickFavorite}
              />
            </List.Item>
          )}
        />
      </div>
    );
  }
}

export default ListGiphy;
