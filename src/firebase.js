import firebase from "firebase/app";
import "firebase/database";
import "firebase/auth";

const config = {
  apiKey: "AIzaSyB1HnMuPEOlFsmN2Z1kQoYQh7WGF9Tpt_4",
  authDomain: "reactworkshopproject.firebaseapp.com",
  databaseURL: "https://reactworkshopproject.firebaseio.com",
  projectId: "reactworkshopproject",
  storageBucket: "reactworkshopproject.appspot.com",
  messagingSenderId: "673467049895"
};

firebase.initializeApp(config);

const database = firebase.database();
const auth = firebase.auth();
const provider = new firebase.auth.FacebookAuthProvider();

export { database, auth, provider };
