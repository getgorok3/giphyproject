import React from "react";
import { Route, Switch } from "react-router-dom";
import MainPage from "./containers/mainPage";
import LoginPage from "./containers/loginPage";
import RegisterPage from "./containers/registerPage";

function Routes() {
  return (
    <div style={{ width: "100%" }}>
      <Switch>
        <Route exact path="/" component={LoginPage} />
        <Route exact path="/register" component={RegisterPage} />
        <Route component={MainPage} />
      </Switch>
    </div>
  );
}

export default Routes;
